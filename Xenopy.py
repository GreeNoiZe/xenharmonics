import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from collections import Counter
import decimal as deci


class Xenopy:
    
    """
    
    genf: generating frequency, the frequency from which the scale is built
    
    """
    
    def __init__(self, genf):
        self.genf = genf

    def eqtemp_scl(self, idiv, ndiv, maxf):
        
        """
        idiv: interval to divide, the interval on which the scale spans (eg: ocatve for the 12-edo)
        ndiv: number of division, the number of notes conatined in the scale
        maxf: max frequency, the greater frequency we want in the scale
        
        scl: dictionnary containg the generated scale and the successive ratios 
        """
        
        scl = dict()
        scl["frequency_list"] = [(self.genf * np.power(idiv, i/(ndiv))) for i in range(0, int((np.log(maxf/self.genf) * ndiv) / np.log(idiv)))]
        scl["successive_ratios"] = [0] + [scl["frequency_list"][i+1]/scl["frequency_list"][i] for i in range(len(scl["frequency_list"]) - 1)]
        
        # Plot tables containing informations on the scale
        # rows = ["frequencies"]
        # cell_text = [scl["frequency_list"]]
        # the_table = plt.table(cellText=cell_text,
                      # rowLabels=rows)
        # plt.show()
        df = pd.DataFrame(scl)
        print(df)
        
        return scl

    def steinhaus_scl(self, I, gen, n):
        
        """
        I: Interval onto which the scale spans
        gen: genrating ratios (eg 3/2, 8/7,etc)
        n:number on iteration on the generator(5 produces a pentatonic sclae)
        
        genr: list of generated ratios
        coef: computed coefficient for a given note in the scale, to be applied to the genf to get the frequency of the given note
        scl: dictionnary containing the generated scale and the successive ratios
        """
        deci.getcontext().prec = 9 # fix the precision in order to ease the numbers comparison
        scl = dict()
        genr = [] #this array will be filled with the successive computed ratios from the generators and wrapped into [1;I]
    
        for i in range(0,n):
            coef = np.power(gen, i)
            if coef <= I:
                genr.append(coef)
            else:
                j = 1
                while coef > I:
                    coef = coef / I
                genr.append(coef)
        # scl["ratios_chart"] = genr        
        genrs = sorted(genr) + [I]
        scl["frequency_list"] = [i * self.genf for i in genrs]
        scl["ratios_chart"] = [0] + [float(deci.Decimal(genrs[i+1])/deci.Decimal(genrs[i])) for i in range(len(genrs) - 1)]
        scl["successive_applied_ratios_"] = genr + [I]
        
        c = Counter(scl["ratios_chart"])
        print(len(c.keys()))
        print(c)
        # scl["number_of_similar_ratios"] = len(c.keys())
        
        df = pd.DataFrame(scl)
        print(df)
        
        return scl
        
        
    def ji_scale(self):
        
        return


a = Xenopy(100)

# sc = a.eqtemp_scl(2, 7, 200)
# b = sc["frequency_list"]
# c = sc["successive_ratios"]

st = a.steinhaus_scl(2, 4/3, 3)
print(st)
# print(st)

